//  We've created an App object (a set of key value pairs) to hold the applcation code.
//  The App object shows how to create a JavaScript object and includes
//  examples of standard programming constructs in JavaScript. 
//  The goal is provide many useful examples in a single code file. 
//
//  When you modify the application, use different ids for your HTML elements.
//  Do not use length and width unless these directly apply to your application. 

var App = {
  launch: launch,
  getTicketBuyerName: getTicketBuyerName,
  getOrganization: getOrganization,
  getNumRows: getNumRows,
  getNumSeats: getNumSeats,
  calculateSeats: calculateSeats,
  calculateEstimatedCount: calculateEstimatedCount,
  showExample: showExample,
  addImage: addImage,
  displayExploreButtons: displayExploreButtons,
  exploreHtml: exploreHtml,
  exploreCode: exploreCode,
  rememberClicks: rememberClicks
}

function launch() {
  const ticketBuyerName = getTicketBuyerName()
  const organization = getOrganization()
  const numRows = getNumRows()
  const numSeats = getNumSeats()
  const seats = calculateSeats(numRows, numSeats)
  const count = calculateEstimatedCount(seats)

  // update page contents 
  $(".displayText").css('display', 'inline-block')  //overwrites display: hidden to make it visible 
  $("#ticketBuyerName").html(ticketBuyerName)
  $("#organization").html(organization)
  $("#numRows").html(numRows)
  $("#numSeats").html(numSeats)
  $("#seats").html(seats)
  $("#count").html(count)
  $("#displayPlace").html('')

  alert("You have about " + seats + " number of seats.")
  alert("You could have about " + count + " seats.")
  $("#count").css("color", "blue")
  $("#count").css("background-color", "yellow")

  showExample(count)
  displayExploreButtons()
}

function cleanInput(inputString) {
  // create a regular expression to find all characters that are not (^) English alphabet chars 
  const re = /[^a-zA-Z]/g
  let justAlpha = inputString.replace(re, '') // strip non-alpha chars out
  return justAlpha
}

function getTicketBuyerName() {
  const MAX_FIRST = 25
  const answer = prompt("Name of ticket buyer?", "John")
  const justAlphaAnswer = cleanInput(answer)

  if (justAlphaAnswer.length > MAX_FIRST) {
    alert('The given answer was greater than ' + MAX_FIRST + '. Using first' + MAX_FIRST + ' characters.')
    justAlphaAnswer = justAlphaAnswer.substr(0, MAX_FIRST)
  }
  return justAlphaAnswer
}

function getOrganization() {
  const MAX_LAST = 30
  const answer = prompt("What is the name of your organization", "Name")
  const justAlphaAnswer = cleanInput(answer)

  if (justAlphaAnswer.length > MAX_LAST) {
    alert('The given answer was greater than ' + MAX_LAST + '. Using first' + MAX_LAST + ' characters.')
    justAlphaAnswer = justAlphaAnswer.substr(0, MAX_LAST)
  }
  return justAlphaAnswer
}

function getNumRows() {
  const DEFAULT_ROW = 5;
  const MAX_ROW = 100;
  const MIN_ROW = 1;
  const answer = prompt("How many rows do you want", DEFAULT_ROW)
  let numRows = parseFloat(answer)
  if (Number.isNaN(numRows)) {
    alert('The given argument is not a number. Using ' + DEFAULT_ROW + '.')
     = DEFAULT_ROW
  }
  else if (numRows > MAX_ROW) {
   alert('The given argument is greater than ' + MAX_ROW + '. Using ' + MAX_ROW + '.')
    numROws = MAX_ROW
  }
  else if (numRows < MIN_ROW) {
    alert('The given argument is less than ' + MIN_ROW + '. Using ' + MIN_ROW + '.')
    numRows = MIN_ROW
  }
  return numRows
}

function getNumSeats() {
  const DEFAULT_SEATS = 5;
  const MAX_SEATS = 100;
  const MIN_SEATS = 1;
  const answer = prompt("How many seats would you like to reserve", DEFAULT_SEATS)
  let numSeats = parseFloat(answer)
  if (Number.isNaN(numSeats)) {
    alert('The given argument is not a number. Using ' + DEFAULT_SEATS + '.')
    numSeats = DEFAULT_SEATS
  }
  else if (numSeats > MAX_SEATS) {
    alert('The given argument is greater than ' + MAX_SEATS + '. Using ' + MAX_SEATS + '.')
    length = MAX_SEATS
  }
  else if (numSeats < MIN_SEATS) {
    alert('The given argument is less than ' + MIN_SEATS + '. Using ' + MIN_SEATS + '.')
    numSeats = MIN_SEATS
  }
  return numSeats
}

function calculateSeats(givenNumRows, givenNumSeats) {
  const MIN_VALUE = 1
  if (typeof givenNumSeats !== 'number' || typeof givenNumRows !== 'number') {
    throw Error('The given argument is not a number')
  }
  if (givenNumSeats < MIN_VALUE) {
    givenNumSeats = MIN_VALUE
  }
  if (givenNumRows < MIN_VALUE) {
    givenNumRows = MIN_VALUE
  }
  // calculate the answer and store in a local variable so we can watch the value
  let seats = givenNumRows * givenNumSeats

  // return the result of our calculation to the calling function
  return seats
}

function calculateEstimatedCount(inputArea) {
  if (typeof inputArea !== 'number') {
    alert('The given argument is not a number')
  }
  let ct = 0
  if (inputArea > 1) {
    ct = inputArea // estimate 1 per square mile
  }
  return ct
}

function showExample(inputCount) {
  for (var i = 0; i < inputCount; i++) {
    addImage(i)
  }
}

function addImage(icount) {
  var imageElement = document.createElement("img")
  imageElement.id = "image" + icount
  imageElement.class = "picture"
  imageElement.style.maxWidth = "90px"
  var displayElement = document.getElementById("displayPlace")
  displayElement.appendChild(imageElement)
  document.getElementById("image" + icount).src = "bearcatLogo.jpg"
}

function displayExploreButtons() {
  $(".displayExploreButtons").css('display', 'block')  //overwrites display: hidden to make it visible 
}

function exploreHtml() {
  alert("Would you like to learn more? \n\n Run the app in Chrome.\n\n" +
    "Right-click on the page, and click Inspect. Click on the Elements tab.\n\n" +
    "Hit CTRL-F and search for displayPlace to see the new image elements you added to the page.\n")
}

function exploreCode() {
  alert("Would you like explore the running code? \n\n Run the app in Chrome.\n\n" +
    "Right-click on the page, and click Inspect. Click on the top-level Sources tab.\n\n" +
    "In the window on the left, click on the .js file.\n\n" +
    "In the window in the center, click on the line number of the getFirstName() call to set a breakpoint.\n\n" +
    "Click on it again to remove the breakpoint, and one more time to turn it back on.\n\n" +
    "Up on the web page, click the main button to launch the \n\n" +
    "Execution of the code will stop on your breakpoint.\n\n" +
    "Hit F11 to step into the getFirstName() function.\n" +
    "Hit F10 to step over the next function call.\n\n" +
    "As you hit F11 and step through your code, the values of local variables appear beside your code - very helpful in debugging.\n\n" +
    "Caution: Hitting F11 in VS Code will make your top-level menu disapper. Hit F11 again to bring it back.\n"
  )
}

function rememberClicks() {
  if (localStorage.getItem("clicks")) { // use getter
    const value = Number(localStorage.clicks) + 1  // or property
    localStorage.setItem("clicks", value)  // use setter
  } else {
    localStorage.clicks = 1 // or property
  }
  s = "You have clicked this button " + localStorage.clicks + " times"
  $("#clicks").html(s) // display forever clicks 
}